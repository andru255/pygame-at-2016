class SingleBall(object):
    def __init__(self, adapter, options={}):
        self.defaults = {
            "radius": 50,
            "color": (0, 255, 255),
            "position": {
                "x": 10,
                "y": 10
            },
            "width": 1
        }

        self.settings = self.defaults.copy()
        self.settings.update(options)

        INIT_POSITION_X = self.settings["position"]["x"]
        INIT_POSITION_Y = self.settings["position"]["y"]

        self.surface = adapter.create_surface({
            "width": INIT_POSITION_X * 2,
            "height": INIT_POSITION_Y * 2
        })

        adapter.draw_circle({
            "surface": self.surface,
            "radius": self.settings["radius"],
            "color": self.settings["color"],
            "position": {
                "x": self.settings["position"]["x"],
                "y": self.settings["position"]["y"]
            },
            "width": self.settings["width"]
        })

        self.surface = self.surface.convert()

    def blit(self, display_surface):
        display_surface.blit(self.surface,(0,0))
