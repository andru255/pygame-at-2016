# -*- coding: utf-8 -*-

from __future__ import print_function, division
import sys, math, pygame
from pygame.locals import *

import imp
module_adapter = imp.load_source('common_adapter', '../common/adapter.py')
pygame_adapter = module_adapter.pygame_adapter()

FPS=60
FPS_CLOCK = pygame.time.Clock()

#importing shapes to use
import ball

#constants
DISPLAY_WIDTH= 400
DISPLAY_HEIGHT= 400
CENTER_SURFACE_X = DISPLAY_WIDTH / 2
CENTER_SURFACE_Y = DISPLAY_HEIGHT / 2

class view(object):
    def __init__(self, options={}):
        self.adapter = pygame_adapter
        self.adapter.initialize()
        self.adapter.set_title("rotate arrow with mouse")

        self.display_surface = self.adapter.create_display({
            "resolution":{
                "width": DISPLAY_WIDTH,
                "height": DISPLAY_HEIGHT
            }
        })

        self.background = self.adapter.create_surface({
            "width": self.display_surface.get_width(),
            "height": self.display_surface.get_height()
        }).convert()

    def paint(self):
        RADIUS_BALL = 100
        self.my_ball = ball.SingleBall(pygame_adapter,{
            "color": (200, 200, 200),
            "radius": RADIUS_BALL,
            "position":{
                "x": int( CENTER_SURFACE_X ),
                "y": int( CENTER_SURFACE_Y )
            },
            "width": 0
        })
        self.my_ball.blit(self.background)

    def render(self):

        angle = 0
        self.paint()

        while True:
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.terminate()
                elif event.type == KEYUP:
                    if event.key == K_ESCAPE:
                        self.terminate()


            #returns a radian angle
            #radian_angle = math.atan2( distance_x , distance_y )

            #change to degree angle
            #degree_angle = angle * ( 180 / math.pi )

            #apply rotation
            surface_updated = pygame.transform.rotate(self.background, 0)

            #bliting!
            self.display_surface.blit(surface_updated,
                (CENTER_SURFACE_X - surface_updated.get_width()/2,
                 CENTER_SURFACE_Y - surface_updated.get_height()/2 + math.sin(angle) * 50))

            #for show the debug line between arrow center and cursor movement
            #pygame.draw.line(self.display_surface, (255, 0, 255),
            #                 (DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2 ),
            #                 (pygame.mouse.get_pos()), 1)

            pygame_adapter.update()
            FPS_CLOCK.tick(FPS)
            angle += 0.1

    def terminate(self):
        pygame.quit()
        sys.exit()


if __name__ == '__main__':
    view().render()
