class Arrow(object):
    def __init__(self, adapter, options={}):
        self.defaults = {
            "color": (0, 255, 255),
            "position": {
                "x": 0,
                "y": 0
            },
            "rotation": 0
        }
        self.settings = self.defaults.copy()
        self.settings.update(options)

        INIT_POSITION_X = self.settings["position"]["x"]
        INIT_POSITION_Y = self.settings["position"]["y"]

        self.surface = adapter.create_surface({
            "width": INIT_POSITION_X * 2,
            "height": INIT_POSITION_Y * 2
        })

        #OTHER WAY
        #coords =[
        #    [INIT_POSITION_X - 50, INIT_POSITION_Y - 25],
        #    [INIT_POSITION_X, INIT_POSITION_Y - 25],
        #    [INIT_POSITION_X, INIT_POSITION_Y - 50],
        #    [INIT_POSITION_X + 50, INIT_POSITION_Y],
        #    [INIT_POSITION_X, INIT_POSITION_Y + 50],
        #    [INIT_POSITION_X, INIT_POSITION_Y + 25],
        #    [INIT_POSITION_X - 50, INIT_POSITION_Y + 25],
        #    [INIT_POSITION_X - 50, INIT_POSITION_Y - 25]
        #]
        #pygame.draw.polygon(self.surface, (255, 250, 0), coords, 2)

        #coords:
        #1
        adapter.draw_coord({
            "surface": self.surface,
            "start_position": {
                "x": INIT_POSITION_X - 50,
                "y": INIT_POSITION_Y - 25
            },
            "end_position": {
                "x": INIT_POSITION_X,
                "y": INIT_POSITION_Y - 25
            }
        })

        #2
        adapter.draw_coord({
            "surface": self.surface,
            "start_position": {
                "x": INIT_POSITION_X,
                "y": INIT_POSITION_Y - 25
            },
            "end_position": {
                "x": INIT_POSITION_X,
                "y": INIT_POSITION_Y - 50
            }
        })

        #3
        adapter.draw_coord({
            "surface": self.surface,
            "start_position": {
                "x": INIT_POSITION_X,
                "y": INIT_POSITION_Y - 50
            },
            "end_position": {
                "x": INIT_POSITION_X + 50,
                "y": INIT_POSITION_Y
            }
        })

        #4
        adapter.draw_coord({
            "surface": self.surface,
            "start_position": {
                "x": INIT_POSITION_X + 50,
                "y": INIT_POSITION_Y
            },
            "end_position": {
                "x": INIT_POSITION_X,
                "y": INIT_POSITION_Y + 50
            }
        })

        #5
        adapter.draw_coord({
            "surface": self.surface,
            "start_position": {
                "x": INIT_POSITION_X,
                "y": INIT_POSITION_Y + 50
            },
            "end_position": {
                "x": INIT_POSITION_X,
                "y": INIT_POSITION_Y + 25
            }
        })

        #6
        adapter.draw_coord({
            "surface": self.surface,
            "start_position": {
                "x": INIT_POSITION_X,
                "y": INIT_POSITION_Y + 25
            },
            "end_position": {
                "x": INIT_POSITION_X - 50,
                "y": INIT_POSITION_Y + 25
            }
        })

        #7
        adapter.draw_coord({
            "surface": self.surface,
            "start_position": {
                "x": INIT_POSITION_X - 50,
                "y": INIT_POSITION_Y + 25
            },
            "end_position": {
                "x": INIT_POSITION_X - 50,
                "y": INIT_POSITION_Y - 25
            }
        })

        self.surface = self.surface.convert()

    def blit(self, display_surface):
        display_surface.blit(self.surface,(0,0))
