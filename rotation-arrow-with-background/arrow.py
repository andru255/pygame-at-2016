class Arrow(object):
    def __init__(self, adapter, options={}):
        self.defaults = {
            "line_color": (0, 255, 255),
            "line_width": 5,
            "background_color": (0, 0, 255),
            "position": {
                "x": 0,
                "y": 0
            },
            "rotation": 0
        }

        self.settings = self.defaults.copy()
        self.settings.update(options)

        INIT_POSITION_X = self.settings["position"]["x"]
        INIT_POSITION_Y = self.settings["position"]["y"]

        self.surface = adapter.create_surface({
            "width": INIT_POSITION_X * 2,
            "height": INIT_POSITION_Y * 2
        })

        #coords
        self.coords =[
            [INIT_POSITION_X - 50, INIT_POSITION_Y - 25],
            [INIT_POSITION_X, INIT_POSITION_Y - 25],
            [INIT_POSITION_X, INIT_POSITION_Y - 50],
            [INIT_POSITION_X + 50, INIT_POSITION_Y],
            [INIT_POSITION_X, INIT_POSITION_Y + 50],
            [INIT_POSITION_X, INIT_POSITION_Y + 25],
            [INIT_POSITION_X - 50, INIT_POSITION_Y + 25],
            [INIT_POSITION_X - 50, INIT_POSITION_Y - 25]
        ]

        adapter.draw_polygon({
            "surface": self.surface,
            "color": self.settings["background_color"],
            "coords": self.coords
        });

        #coords:
        #1
        adapter.draw_coord({
            "surface": self.surface,
            "width": self.settings["line_width"],
            "color": self.settings["line_color"],
            "start_position": {
                "x": self.coords[0][0],
                "y": self.coords[0][1]
            },
            "end_position": {
                "x": self.coords[1][0],
                "y": self.coords[1][1]
            }
        })

        #2
        adapter.draw_coord({
            "surface": self.surface,
            "width": self.settings["line_width"],
            "color": self.settings["line_color"],
            "start_position": {
                "x": self.coords[1][0],
                "y": self.coords[1][1]
            },
            "end_position": {
                "x": self.coords[2][0],
                "y": self.coords[2][1]
            }
        })

        #3
        adapter.draw_coord({
            "surface": self.surface,
            "width": self.settings["line_width"],
            "color": self.settings["line_color"],
            "start_position": {
                "x": self.coords[2][0],
                "y": self.coords[2][1]
            },
            "end_position": {
                "x": self.coords[3][0],
                "y": self.coords[3][1]
            }
        })

        #4
        adapter.draw_coord({
            "surface": self.surface,
            "width": self.settings["line_width"],
            "color": self.settings["line_color"],
            "start_position": {
                "x": self.coords[3][0],
                "y": self.coords[3][1]
            },
            "end_position": {
                "x": self.coords[4][0],
                "y": self.coords[4][1]
            }
        })

        #5
        adapter.draw_coord({
            "surface": self.surface,
            "width": self.settings["line_width"],
            "color": self.settings["line_color"],
            "start_position": {
                "x": self.coords[4][0],
                "y": self.coords[4][1]
            },
            "end_position": {
                "x": self.coords[5][0],
                "y": self.coords[5][1]
            }
        })

        #6
        adapter.draw_coord({
            "surface": self.surface,
            "width": self.settings["line_width"],
            "color": self.settings["line_color"],
            "start_position": {
                "x": self.coords[5][0],
                "y": self.coords[5][1]
            },
            "end_position": {
                "x": self.coords[6][0],
                "y": self.coords[6][1]
            }
        })

        #7
        adapter.draw_coord({
            "surface": self.surface,
            "width": self.settings["line_width"],
            "color": self.settings["line_color"],
            "start_position": {
                "x": self.coords[6][0],
                "y": self.coords[6][1]
            },
            "end_position": {
                "x": self.coords[7][0],
                "y": self.coords[7][1]
            }
        })

        self.surface = self.surface.convert()

    def blit(self, display_surface):
        display_surface.blit(self.surface,(0,0))
