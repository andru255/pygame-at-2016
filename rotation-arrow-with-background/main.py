# -*- coding: utf-8 -*-

from __future__ import print_function, division
import sys, math, pygame
from pygame.locals import *

import imp
module_adapter = imp.load_source('common_adapter', '../common/adapter.py')
pygame_adapter = module_adapter.pygame_adapter()

#importing shapes to use
import arrow

#constants
DISPLAY_WIDTH= 640
DISPLAY_HEIGHT= 480
CENTER_SURFACE_X = DISPLAY_WIDTH / 2
CENTER_SURFACE_Y = DISPLAY_HEIGHT / 2

class view(object):
    def __init__(self, options={}):
        self.adapter = pygame_adapter
        self.adapter.initialize()
        self.adapter.set_title("rotate arrow with mouse")

        self.display_surface = self.adapter.create_display({
            "resolution":{
                "width": DISPLAY_WIDTH,
                "height": DISPLAY_HEIGHT
            }
        })

        self.background = self.adapter.create_surface({
            "width": self.display_surface.get_width(),
            "height": self.display_surface.get_height()
        }).convert()


    def paint(self):
        self.my_arrow = arrow.Arrow(self.adapter,{
            "position":{
                "x": CENTER_SURFACE_X,
                "y": CENTER_SURFACE_Y
            }
        })
        self.my_arrow.blit(self.background)

    def test_paint(self):
        #---L ARM DRAW

        INIT_POSITION_X = CENTER_SURFACE_X
        INIT_POSITION_Y = CENTER_SURFACE_Y

        self.adapter.draw_polygon({
            "surface": self.background,
            "color": (255, 0, 0),
            "coords": ((243,182),(242,176),(231,167),
             (226,168),(218,181),(204,190),(204,200),(212,208),
             (219,208),(232,197))
        })
        #arm n hand
        #pygame.draw.polygon(
        #    self.background,
        #    (255,255,255),
        #    ((243,182),(242,176),(231,167),
        #     (226,168),(218,181),(204,190),(204,200),(212,208),
        #     (219,208),(232,197))
        #)
        #arm fur

    def render(self):

        self.paint()
        #self.test_paint()

        while True:
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.terminate()
                elif event.type == KEYUP:
                    if event.key == K_ESCAPE:
                        self.terminate()

            mouse_pos_x, mouse_pos_y = pygame.mouse.get_pos()
            distance_x = mouse_pos_x - CENTER_SURFACE_X
            distance_y = mouse_pos_y - CENTER_SURFACE_Y
            radian_angle = math.atan2( distance_x , distance_y )
            degree_angle = radian_angle * ( 180 / math.pi )

            surface_updated = pygame.transform.rotate(self.background, degree_angle - 90)

            self.display_surface.blit(surface_updated,
                (CENTER_SURFACE_X - surface_updated.get_width()/2,
                 CENTER_SURFACE_Y - surface_updated.get_height()/2))

            #for show the debug line between arrow center and cursor movement
            pygame.draw.line(self.display_surface, (255, 0, 255),
                             (DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2 ),
                             (pygame.mouse.get_pos()), 1)

            pygame_adapter.update()

    def terminate(self):
        pygame.quit()
        sys.exit()


if __name__ == '__main__':
    view().render()
