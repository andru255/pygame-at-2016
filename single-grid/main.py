# -*- coding: utf-8 -*-

from __future__ import print_function, division
import sys, math, pygame
from pygame.locals import *

import adapter

FPS=30
FPS_CLOCK = pygame.time.Clock()
WIN_WIDTH=800
WIN_HEIGHT=600

COLOR_BLACK = ( 0, 0, 0)
COLOR_WHITE = ( 255, 255, 255)
COLOR_BLUE  = ( 0, 0, 255)
COLOR_RED  =  ( 255, 0, 0)

DISTANCE_BETWEEN_ROWS=20
DISTANCE_BETWEEN_COLUMNS=20

DISPLAY_SURFACE = adapter.create_surface({
    "resolution":{
        "width": WIN_WIDTH,
        "height": WIN_HEIGHT
    }
})

def main():
    adapter.initialize()
    adapter.set_title("Single grid")
    showGrid()
    while True:
        showThingsOnGrid()

def showGrid():
    ROWS = int( getNumROWSOnScreen() )
    COLUMNS = int( getNumCOLUMNSOnScreen() )

    for row in range(0, ROWS):
        start_position = {
                "x": 0,
                "y": DISTANCE_BETWEEN_ROWS * row
        }
        adapter.drawCoord({
            "surface": DISPLAY_SURFACE,
            "color": COLOR_RED,
            "start_position": start_position,
            "end_position":{
                "x": WIN_WIDTH,
                "y": DISTANCE_BETWEEN_ROWS * row
            }
        })
        adapter.printLabel({
            "surface": DISPLAY_SURFACE,
            "color": COLOR_WHITE,
            "text": DISTANCE_BETWEEN_ROWS * row,
            "position": start_position
        })

    for column in range(0, COLUMNS):
        start_position = {
            "x": DISTANCE_BETWEEN_COLUMNS * column,
            "y": 0
        }
        adapter.drawCoord({
            "surface": DISPLAY_SURFACE,
            "color": COLOR_BLUE,
            "start_position": start_position,
            "end_position":{
                "x": DISTANCE_BETWEEN_COLUMNS * column,
                "y": WIN_HEIGHT
            }
        })
        adapter.printLabel({
            "surface": DISPLAY_SURFACE,
            "color": COLOR_WHITE,
            "text": DISTANCE_BETWEEN_COLUMNS * column,
            "position": start_position
        })

def getNumROWSOnScreen():
    return math.ceil( WIN_HEIGHT / DISTANCE_BETWEEN_ROWS )

def getNumCOLUMNSOnScreen():
    return math.ceil( WIN_WIDTH / DISTANCE_BETWEEN_COLUMNS )


def showThingsOnGrid():
    start = 0
    x = 10
    increase = 0.05

    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                terminate()
            elif event.type == KEYUP:
                if event.key == K_ESCAPE:
                    terminate()

        DISPLAY_SURFACE.fill(COLOR_BLACK)
        #showGrid()

        y = math.sin(start) * 10
        adapter.printLabel({
            "surface": DISPLAY_SURFACE,
            "text": "cool!",
            "size": 30,
            "color": (0, 135, 0),
            "position":{
                "x": x,
                "y": y
            }
        })

        adapter.update()
        FPS_CLOCK.tick(FPS)
        start += increase

def terminate():
    pygame.quit()
    sys.exit()

if __name__ == '__main__':
    main()
