import pygame
from pygame.locals import *

def initialize():
    pygame.init()

def update():
    pygame.display.update()

def create_surface(options):
    defaults = {
        "resolution":{
            "width": 100,
            "height": 100
        },
        "flags": 0,
        "depth": 32
    }
    settings = defaults.copy()
    settings.update(options)


    return pygame.display.set_mode(
        (settings["resolution"]["width"], settings["resolution"]["height"]),
        settings["flags"],
        settings["depth"]
    )

def set_title(title):
    pygame.display.set_caption(title)

def drawCoord(options):
    '''
        pygame.draw.line(DISPLAY_SURFACE, COLOR_RED, (0, 0), (120, 60), 1)
    '''

    defaults = {
        "color": (255, 255, 255),
        "start_position":{
            "x": 10,
            "y": 10,
        },
        "end_position":{
            "x": 20,
            "y": 20,
        },
        "width": 1
    }
    settings = defaults.copy()
    settings.update(options)

    pygame.draw.line(
        settings["surface"],
        settings["color"],
        (settings["start_position"]["x"], settings["start_position"]["y"]),
        (settings["end_position"]["x"], settings["end_position"]["y"]),
        settings["width"]
    )

def printLabel(options):
    defaults = {
        "surface": None,
        "family": None,
        "size": 14,
        "text": "foo bar",
        "color": (255, 255, 255),
        "position": {
            "x": 0,
            "y": 0
        }
    }
    settings = defaults.copy()
    settings.update(options)

    FONT_BASE = pygame.font.Font(settings["family"], settings["size"])
    text = FONT_BASE.render(str( settings[ "text" ] ), 1, settings["color"])
    options["surface"].blit(text, (settings["position"]["x"], settings["position"]["y"]))
