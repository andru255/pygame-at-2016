# -*- coding: utf-8 -*-

from __future__ import print_function, division
import sys, math, pygame
from pygame.locals import *

import imp
# importing adapter
module_adapter = imp.load_source('common_adapter', '../common/adapter.py')
pygame_adapter = module_adapter.pygame_adapter()

#importing shapes to use
import ball

#constants
DISPLAY_WIDTH= 800
DISPLAY_HEIGHT= 600

class view(object):
    def __init__(self, options={}):
        self.adapter = pygame_adapter
        self.adapter.initialize()
        self.adapter.set_title("shapes in OOP")
        self.display_surface = self.adapter.create_display({
            "resolution":{
                "width": DISPLAY_WIDTH,
                "height": DISPLAY_HEIGHT
            }
        })
        self.background = self.adapter.create_surface({
            "width": self.display_surface.get_width(),
            "height": self.display_surface.get_height()
        }).convert()
        self.background.fill(( 255, 255, 255 ))

    def paint(self):
        RADIUS_BALL = 10
        my_ball = ball.Ball(pygame_adapter,{
            "radius": RADIUS_BALL,
            "position":{
                "x": RADIUS_BALL,
                "y": RADIUS_BALL
            },
            "width": 1
        })

        #for white background
        #self.display_surface.blit(self.background, (0, 0))
        my_ball.blit(self.display_surface)

    def render(self):

        self.paint()

        while True:
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.terminate()
                elif event.type == KEYUP:
                    if event.key == K_ESCAPE:
                        self.terminate()
            #fundamental for drawing
            self.adapter.flip()
            #self.surface.blit(self.background, (0, 0))

    def terminate(self):
        pygame.quit()
        sys.exit()


if __name__ == '__main__':
    view().render()
