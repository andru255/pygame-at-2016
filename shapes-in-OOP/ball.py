class Ball(object):
    def __init__(self, adapter, options={}):
        self.defaults = {
            "radius": 50,
            "color": (0, 255, 255),
            "position": {
                "x": 10,
                "y": 10
            },
            "width": 10
        }
        self.settings = self.defaults.copy()
        self.settings.update(options)

        self.surface = adapter.create_surface({
            "width": 2 * self.settings["radius"],
            "height": 2 * self.settings["radius"]
        })

        adapter.draw_circle({
            "surface": self.surface,
            "radius": self.settings["radius"],
            "color": self.settings["color"],
            "position": {
                "x": self.settings["position"]["x"],
                "y": self.settings["position"]["y"]
            },
            "width": self.settings["width"]
        })

        self.surface = self.surface.convert()

    def blit(self, display_surface):
        display_surface.blit(self.surface,
                        (self.settings["position"]["x"],
                         self.settings["position"]["y"]))
