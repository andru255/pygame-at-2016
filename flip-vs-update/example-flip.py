# -*- coding: utf-8 -*-

from __future__ import print_function, division
import sys, math, pygame
from pygame.locals import *

import imp
# importing adapter
module_adapter = imp.load_source('common_adapter', '../common/adapter.py')
adapter = module_adapter.pygame_adapter()

DISPLAY_SURFACE = adapter.create_display({
    "resolution":{
        "width": 300,
        "height": 300
    }
})

def terminate():
    pygame.quit()
    sys.exit()

while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            terminate()
        elif event.type == KEYUP:
            if event.key == K_ESCAPE:
                terminate()
    # http://www.pygame.org/docs/ref/display.html#pygame.display.flip
    # Update the full display Surface to the screen
    adapter.flip()
