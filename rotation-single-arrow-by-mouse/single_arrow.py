class SingleArrow(object):
    def __init__(self, adapter, options={}):
        self.defaults = {
            "line_color": (0, 255, 255),
            "line_width": 5,
            "background_color": (0, 0, 255),
            "position": {
                "x": 0,
                "y": 0
            },
            "rotation": 0
        }

        self.settings = self.defaults.copy()
        self.settings.update(options)

        INIT_POSITION_X = self.settings["position"]["x"]
        INIT_POSITION_Y = self.settings["position"]["y"]

        self.surface = adapter.create_surface({
            "width": INIT_POSITION_X * 2,
            "height": INIT_POSITION_Y * 2
        })

        #coords
        self.coords =[
            [INIT_POSITION_X - 50, INIT_POSITION_Y - 25],
            [INIT_POSITION_X, INIT_POSITION_Y - 25],
            [INIT_POSITION_X, INIT_POSITION_Y - 50],
            [INIT_POSITION_X + 50, INIT_POSITION_Y],
            [INIT_POSITION_X, INIT_POSITION_Y + 50],
            [INIT_POSITION_X, INIT_POSITION_Y + 25],
            [INIT_POSITION_X - 50, INIT_POSITION_Y + 25],
            [INIT_POSITION_X - 50, INIT_POSITION_Y - 25]
        ]

        adapter.draw_polygon({
            "surface": self.surface,
            "color": self.settings["background_color"],
            "coords": self.coords
        });

        adapter.draw_polygon({
            "surface": self.surface,
            "color": self.settings["line_color"],
            "coords": self.coords,
            "width": 2
        });

        self.surface = self.surface.convert()

    def blit(self, display_surface):
        display_surface.blit(self.surface,(0,0))
