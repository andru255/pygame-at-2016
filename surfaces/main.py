# -*- coding: utf-8 -*-

from __future__ import print_function, division
import sys, math, pygame
from pygame.locals import *

import imp
module_adapter = imp.load_source('common_adapter', '../common/adapter.py')
pygame_adapter = module_adapter.pygame_adapter()

#constants
DISPLAY_WIDTH= 300
DISPLAY_HEIGHT= 300


class view(object):
    def __init__(self, options={}):
        self.adapter = pygame_adapter
        self.adapter.initialize()
        self.adapter.set_title("surfaces!")

        self.screen = self.adapter.create_display({
            "resolution":{
                "width": DISPLAY_WIDTH,
                "height": DISPLAY_HEIGHT
            }
        })

        self.single_surface = self.adapter.create_surface({
            "width": 50,
            "height": 50
        }).convert()


    def paint_on_screen(self):
        pygame.draw.circle(self.screen, (0, 0, 255), (25, 25), 25)

    def paint_on_single_surface(self):
        self.single_surface.fill((255, 255, 0))
        pygame.draw.circle(self.single_surface, (255, 0, 180), (25, 25), 25)
        self.screen.blit(self.single_surface, (150, 0))

    def render(self):

        self.paint_on_single_surface()
        self.paint_on_screen()

        while True:
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.terminate()
                elif event.type == KEYUP:
                    if event.key == K_ESCAPE:
                        self.terminate()

            pygame_adapter.update()

    def terminate(self):
        pygame.quit()
        sys.exit()


if __name__ == '__main__':
    view().render()
