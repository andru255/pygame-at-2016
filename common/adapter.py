from __future__ import print_function, division
import pygame
from pygame.locals import *

class pygame_adapter(object):
    def initialize(self):
        pygame.init()

    def update(self):
        pygame.display.update()

    def flip(self):
        pygame.display.flip()

    def create_display(self, options={}):
        defaults = {
            "resolution":{
                "width": 100,
                "height": 100
            },
            "flags": 0,
            "depth": 32
        }
        settings = defaults.copy()
        settings.update(options)


        return pygame.display.set_mode(
            (settings["resolution"]["width"], settings["resolution"]["height"]),
            settings["flags"],
            settings["depth"]
        )

    def create_surface(self, options={}):
        defaults = {
            "width": 100,
            "height": 100
        }
        settings = defaults.copy()
        settings.update(options)

        return pygame.Surface((settings["width"], settings["height"]))

    def set_title(self, title):
        pygame.display.set_caption(title)

    def draw_coord(self, options={}):
        '''
            pygame.draw.line(DISPLAY_SURFACE, COLOR_RED, (0, 0), (120, 60), 1)
        '''
        defaults = {
            "color": (255, 255, 255),
            "start_position":{
                "x": 10,
                "y": 10,
            },
            "end_position":{
                "x": 20,
                "y": 20,
            },
            "width": 1
           }
        settings = defaults.copy()
        settings.update(options)

        pygame.draw.line(
            settings["surface"],
            settings["color"],
            (settings["start_position"]["x"], settings["start_position"]["y"]),
            (settings["end_position"]["x"], settings["end_position"]["y"]),
            settings["width"]
        )


    def draw_polygon(self, options={}):
        '''
            pygame.draw.polygon(DISPLAY_SURFACE, color, coords, width)
        '''
        defaults = {
            "surface": None,
            "color": (0, 255, 255),
            "coords": [[10, 10] , [20, 10] , [20, 30]],
            "width": 0
        }

        settings = defaults.copy()
        settings.update(options)

        pygame.draw.polygon(
            settings["surface"],
            settings["color"],
            settings["coords"],
            settings["width"]
        )

    def draw_circle(self, options={}):
        '''
            pygame.draw.circle(DISPLAY_SURFACE, color, (x, y), radius, width)
        '''
        defaults = {
            "surface": None,
            "color": (255, 255, 255),
            "position":{
                "x": 10,
                "y": 10,
            },
            "radius": 10,
            "width": 0
        }
        settings = defaults.copy()
        settings.update(options)

        pygame.draw.circle(
            settings["surface"],
            settings["color"],
            (settings["position"]["x"], settings["position"]["y"]),
            settings["radius"],
            settings["width"]
        )


    def print_label(self, options={}):
        defaults = {
            "surface": None,
            "family": None,
            "size": 14,
            "text": "foo bar",
            "color": (255, 255, 255),
            "position": {
                "x": 0,
                "y": 0
            }
           }
        settings = defaults.copy()
        settings.update(options)

        FONT_BASE = pygame.font.Font(settings["family"], settings["size"])
        text = FONT_BASE.render(str( settings[ "text" ] ), 1, settings["color"])
        options["surface"].blit(text, (settings["position"]["x"], settings["position"]["y"]))
